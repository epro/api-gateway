import should from "should";
import assert from "assert";
import UserEntity from "./../../../src/Domain/User/Entities/UserEntity";
import { UserLogin } from "./../../../src/Domain/User/Services/UserLogin";
import { NotFoundException } from "./../../../src/Application/Exceptions/NotFoundException";

describe("USER LOGIN TEST", () => {

	before(done => {
		require("./../../DBConnection");
		done();
	});

	let result = "";
	describe("# ADMIN LOGIN", () => {
		describe("Positive tests", () => {
			before(done => {
				UserEntity.findOne({
					role: "ADMIN"
				}).select("username").lean().then(user => {
					new UserLogin().attempt(user.username, "rahasia123").then(user => {
						result = user;
						done();
					});
				});
			});

			it("Should return result as an object", done => {
	            should(result).be.an.Object();
	            done();
	        });

	        it("Should return a valid response body", done => {
	        	should(result).have.property("id");
	            should(result).have.property("name").which.is.a.String();
	            should(result).have.property("username").which.is.a.String();
	            should(result).have.property("role").equal("ADMIN");
	            should(result).have.property("token").which.is.a.String();
	            done();
	        });
	    });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		new UserLogin().attempt("credentials.admin.username", "blablabla").catch(exception => {
        			error = exception;
        			done();
        		});
        	});

        	it("Should throw NotFoundException if the username or password is not valid", done => {
	        	assert.throws(() => {
	        		throw new NotFoundException("Invalid username or password!");
	        	}, error);
	        	done();
	        });

	        it("Should have 'Invalid username or password!' error message", done => {
	        	should(error.message).equal("Invalid username or password!");
	        	done();
	        });
        });
	});

	describe("# STUDENT LOGIN", () => {
		describe("Positive tests", () => {
			before(done => {
				UserEntity.findOne({
					role: "STUDENT"
				}).select("username").lean().then(user => {
					new UserLogin().attempt(user.username, "rahasia123").then(user => {
						result = user;
						done();
					});
				});
			});

			it("Should return result as an object", done => {
	            should(result).be.an.Object();
	            done();
	        });

	        it("Should return a valid response body", done => {
	        	should(result).have.property("id");
	            should(result).have.property("name").which.is.a.String();
	            should(result).have.property("username").which.is.a.String();
	            should(result).have.property("email").which.is.a.String();
	            should(result).have.property("phone").which.is.a.String();
	            should(result).have.property("role").equal("STUDENT");
	            should(result).have.property("token").which.is.a.String();
	            done();
	        });
		});

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		new UserLogin().attempt("credentials.student.username", "blablabla").catch(exception => {
        			error = exception;
        			done();
        		});
        	});

        	it("Should throw NotFoundException if the username or password is not valid", done => {
	        	assert.throws(() => {
	        		throw new NotFoundException("Invalid username or password!");
	        	}, error);
	        	done();
	        });

	        it("Should have 'Invalid username or password!' error message", done => {
	        	should(error.message).equal("Invalid username or password!");
	        	done();
	        });
        });
	});

	describe("# TEACHER LOGIN", () => {
		describe("Positive tests", () => {
			before(done => {
				UserEntity.findOne({
					role: "TEACHER"
				}).select("username").lean().then(user => {
					new UserLogin().attempt(user.username, "rahasia123").then(user => {
						result = user;
						done();
					});
				});
			});

			it("Should return result as an object", done => {
	            should(result).be.an.Object();
	            done();
	        });

	        it("Should return a valid response body", done => {
	        	should(result).have.property("id");
	            should(result).have.property("name").which.is.a.String();
	            should(result).have.property("username").which.is.a.String();
	            should(result).have.property("email").which.is.a.String();
	            should(result).have.property("phone").which.is.a.String();
	            should(result).have.property("role").equal("TEACHER");
	            should(result).have.property("token").which.is.a.String();
	            done();
	        });
		});

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		new UserLogin().attempt("credentials.teacher.username", "blablabla").catch(exception => {
        			error = exception;
        			done();
        		});
        	});

        	it("Should throw NotFoundException if the username or password is not valid", done => {
	        	assert.throws(() => {
	        		throw new NotFoundException("Invalid username or password!");
	        	}, error);
	        	done();
	        });

	        it("Should have 'Invalid username or password!' error message", done => {
	        	should(error.message).equal("Invalid username or password!");
	        	done();
	        });
        });
	});
});