import { UserRegister } from "./../../../src/Domain/User/Services/UserRegister";
import { ValidationError } from "mongoose";
import { UserContract } from "./../../../src/Application/Contracts/UserContract";
import UserEntity from "./../../../src/Domain/User/Entities/UserEntity";
import should from "should";

describe("USER REGISTRATION TEST", () => {
	let admin_count = 0;
	let userData = {};
	let student_count = 0;
	let teacher_count = 0;

	before(done => {
		require("./../../DBConnection");

		UserEntity.countDocuments({role: "ADMIN"}).then(admin => {
			admin_count = admin++;
			userData.admin = new UserContract({
				name: `ADMIN ${admin_count}`,
				phone: `08123456789${admin_count}`,
				email: `admin${admin_count}@mail.com`,
				role: "ADMIN",
				username: `admin${admin_count}`,
				password: "rahasia123"
			});
		}).then(() => {
			UserEntity.countDocuments({role: "STUDENT"}).then(student => {
				student_count = student++;
				userData.student = new UserContract({
					name: `STUDENT ${student_count}`,
					phone: `08123456789${student_count}`,
					email: `student${student_count}@mail.com`,
					role: "STUDENT",
					username: `student${student_count}`,
					password: "rahasia123"
				});
			});
		}).then(() => {
			UserEntity.countDocuments({role: "TEACHER"}).then(teacher => {
				teacher_count = teacher++;
				userData.teacher = new UserContract({
					name: `TEACHER ${teacher_count}`,
					phone: `08123456789${teacher_count}`,
					email: `teacher{teacher_count}@mail.com`,
					role: "TEACHER",
					username: `teacher{teacher_count}`,
					password: "rahasia123"
				});
			});

			done();
		});
	});

	let result = "";

	/**
	 * Register admin
	 */
	describe("# REGISTER ADMIN", () => {
		describe("Positive tests", () => {
			before(done => {
				new UserRegister().register(userData.admin).then(user => {
					result = user;
					admin_count++;
					userData.admin.name = `ADMIN ${admin_count}`;
					userData.admin.username = `admin${admin_count}`;
					done();
				});
			});

			it("Should return result as an object", done => {
	            should(result).be.an.Object();
	            done();
	        });

	        it("Should return a valid response body", done => {
	        	should(result).have.property("id");
	            should(result).have.property("name").which.is.a.String();
	            should(result).have.property("username").which.is.a.String();
	            should(result).have.property("role").equal("ADMIN");
	            should(result).have.property("token").which.is.a.String();
	            done();
	        });
	    });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		delete userData.admin.profile.name;
        		new UserRegister().register(userData.admin).catch(exception => {
					error = exception;
					done();
				});
        	});

        	it("Should throw error if the data is not valid", done => {
	        	should(error.name).equal("ValidationError");
	        	done();
	        });
        });
	});

	/**
	 * Register student
	 */
	describe("# REGISTER STUDENT", () => {
		describe("Positive tests", () => {
			before(done => {
				new UserRegister().register(userData.student).then(user => {
					result = user;
					student_count++;
					userData.student.name = `STUDENT ${student_count}`;
					userData.student.username = `student${student_count}`;
					userData.student.phone = `0812345678${student_count}`;
					userData.student.email = `student${student_count}@mail.com`;
					done();
				});
			});

			it("Should return result as an object", done => {
	            should(result).be.an.Object();
	            done();
	        });

	        it("Should return a valid response body", done => {
	        	should(result).have.property("id");
	            should(result).have.property("name").which.is.a.String();
	            should(result).have.property("username").which.is.a.String();
	            should(result).have.property("email").which.is.a.String();
	            should(result).have.property("phone").which.is.a.String();
	            should(result).have.property("role").equal("STUDENT");
	            should(result).have.property("token").which.is.a.String();
	            done();
	        });
	    });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		delete userData.student.profile.name;
        		new UserRegister().register(userData.student).catch(exception => {
					error = exception;
					done();
				});
        	});

        	it("Should throw error if the data is not valid", done => {
	        	should(error.name).equal("ValidationError");
	        	done();
	        });
        });
	});

	/**
	 * Register teacher
	 */
	describe("# REGISTER TEACHER", () => {
		describe("Positive tests", () => {
			before(done => {
				new UserRegister().register(userData.teacher).then(user => {
					result = user;
					teacher_count++;
					userData.teacher.name = `TEACHER ${teacher_count}`;
					userData.teacher.username = `teacher${teacher_count}`;
					userData.teacher.phone = `0812345678${teacher_count}`;
					userData.teacher.email = `teacher${teacher_count}@mail.com`;
					done();
				});
			});

			it("Should return result as an object", done => {
	            should(result).be.an.Object();
	            done();
	        });

	        it("Should return a valid response body", done => {
	        	should(result).have.property("id");
	            should(result).have.property("name").which.is.a.String();
	            should(result).have.property("username").which.is.a.String();
	            should(result).have.property("email").which.is.a.String();
	            should(result).have.property("phone").which.is.a.String();
	            should(result).have.property("role").equal("TEACHER");
	            should(result).have.property("token").which.is.a.String();
	            done();
	        });
		});

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		delete userData.teacher.profile.name;
        		new UserRegister().register(userData.teacher).catch(exception => {
					error = exception;
					done();
				});
        	});

        	it("Should throw error if the data is not valid", done => {
	        	should(error.name).equal("ValidationError");
	        	done();
	        });
        });
	});
});
