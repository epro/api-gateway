import should from "should";
import assert from "assert";
import UserEntity from "./../../../src/Domain/User/Entities/UserEntity";
import { UserRepository } from "./../../../src/Infrastructures/Repositories/UserRepository";
import { NotFoundException } from "./../../../src/Application/Exceptions/NotFoundException";

describe("USE REPOSITORY TEST", () => {

	before(done => {
		require("./../../DBConnection");
		done();
	});

	describe("Set user role", () => {
		let role = {};
		
		before(done => {
			role = new UserRepository().setRole();
			done();
		});

		it("Should return object with _role property", done => {
			should(role).have.property("_role").which.is.a.String();
			done();
		});

		it("Should return admin as a default role", done => {
			should(role._role).equal("ADMIN");
			done();
		});

		it("Should returns the same role as parameter", done => {
			const sent_role = "teacher";
			role = new UserRepository().setRole(sent_role);

			should(role._role).equal(sent_role.toUpperCase());
			done();
		});
	});

	describe("Select fields", () => {
		let selectedFields = "";

		before(done => {
			selectedFields = new UserRepository().selectField();
			done();
		});

		it("Should return empty object as a default value", done => {
			should(selectedFields._fields).be.an.Object();
			should(selectedFields._fields).be.empty();
			done();
		});

		it("Should returns the same fields as parameter", done => {
			const fields = "_id name username";
			selectedFields = new UserRepository().selectField(fields);

			should(selectedFields._fields).be.a.String();
			should(selectedFields._fields).equal(fields);
			done();
		});
	});

	describe("Get users", () => {
		let users = [];

		before(done => {
			new UserRepository()
				.setRole("admin")
				.selectField("_id profile.name profile.phone profile.email")
				.get()
				.then(result => {
					result.build().then(list_users => {
						users = list_users;
						done();
					});
				});
		});

		it("Should returns user object", done => {
			should(users).be.an.Object();
			done();
		});
	});

	describe("User profile", () => {
		let user = "";
		let role = "";

		before(done => {
			UserEntity.findOne().select("_id username role").lean().then(result => {
				role = user.role;

				new UserRepository()
					.setRole(result.role)
					.selectField("_id profile.name profile.phone profile.email")
					.profile(result._id).then(result => {
						user = result;
						done();
					});
			});
		});

		it("Should return object user profile", done => {
			should(user).be.an.Object();
			done();
		});

		it("Should throws NotFoundException if user data couldn't be found", done => {
			new UserRepository()
				.setRole("teacher")
				.selectField("_id profile.name profile.phone profile.email")
				.profile(user._id).catch(error => {
					assert.throws(() => {
		        		throw new NotFoundException("The data you're looking for couldn't be found!");
		        	}, error);
					done();
				});
		});
	});
});