import mongoose from "mongoose";

require('dotenv').load();

before(() => {
    mongoose.set('useFindAndModify', false);
    mongoose.connect(process.env.MONGO_TEST_URI, {
        useNewUrlParser: true,
	    useCreateIndex: true
    });
});

after(() => {
	mongoose.connection.db.dropDatabase(function () {
        mongoose.connection.close();
    });
});