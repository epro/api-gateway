import mongoose from "mongoose";

/**
 * Connect to MongoDB server
 * @namespace process.env.MONGO_URI
 */
export default () => {
	mongoose.connect(process.env.MONGO_URI, {
	    useNewUrlParser: true,
	    useCreateIndex: true,
	    useFindAndModify: false
	}).then(() => {
	    console.log("Successfully connected to DB!");
	}, (err) => {
	    console.log("An error occurred while connecting to DB!");
	    throw new Error(err);
	});
}