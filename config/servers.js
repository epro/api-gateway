require("dotenv").config();

const servers = {
    local: {
        class: [{
            url: "http://localhost:4000",
            status: true
        }, {
            url: "http://localhost:4001",
            status: true
        }, {
            url: "http://localhost:4002",
            status: true
        }],
        test: [{
            url: "http://localhost:5000",
            status: true
        }, {
            url: "http://localhost:5001",
            status: true
        }, {
            url: "http://localhost:5002",
            status: true
        }],
        material: [{
            url: "http://localhost:7000",
            status: true
        }, {
            url: "http://localhost:7001",
            status: true
        }, {
            url: "http://localhost:7002",
            status: true
        }]
    },
    production: {
        class: [{
            url: process.env.CLASS_SERVICE_PROD_1,
            status: true
        }, {
            url: process.env.CLASS_SERVICE_PROD_2,
            status: true
        }, {
            url: process.env.CLASS_SERVICE_PROD_3,
            status: true
        }],
        test: [{
            url: process.env.TEST_SERVICE_PROD_1,
            status: true
        }, {
            url: process.env.TEST_SERVICE_PROD_2,
            status: true
        }, {
            url: process.env.TEST_SERVICE_PROD_3,
            status: true
        }],
        material: [{
            url: process.env.MATERIAL_SERVICE_PROD_1,
            status: true
        }, {
            url: process.env.MATERIAL_SERVICE_PROD_2,
            status: true
        }, {
            url: process.env.MATERIAL_SERVICE_PROD_3,
            status: true
        }]
    }
};

module.exports = servers;