import restify from "restify";
import mongoose from "mongoose";
import DBConnection from "./config/database"; 
import discover from "@interfaces/Http/discover";

const server = restify.createServer({
	name: "api-gateway",
	ignoreTrailingSlash: true
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(restify.plugins.pre.sanitizePath());

require("dotenv").load();

/**
 * Connect to MongoDB server
 */
DBConnection();

const router = require("@interfaces/Http/Routes");
router.applyRoutes(server);

server.listen(process.env.PORT || 3000, () => {
	discover();
	console.log(`API Gateway running on port ${process.env.PORT || 3000}`);
});