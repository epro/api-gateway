import _ from "lodash";
import UserEntity from "@user/Entities/UserEntity";
import { NotFoundException } from "@exceptions/NotFoundException";

export class UserRepository {

    constructor() {
        this._fields = {};
        this.users = [];
    }

    selectField(fields = {}) {
        this._fields = fields;
        return this;
    }

    setRole(role = "admin") {
        this._role = role.toUpperCase();
        return this;
    }

    async profile(id) {
        return UserEntity.findOne({
            _id: id,
            role: this._role
        }).select(this._fields).lean().then(user => {
            if (user !== null) {
                return {
                    _id: user._id,
                    ...user.profile
                };
            } else {
                throw new NotFoundException("The data you're looking for couldn't be found!");
            }
        });
    }

    /**
     * Find user by credential
     * 
     * @param string username 
     * @param string password
     */
    async findByCredential(username, password) {
        return UserEntity.findOne({
            username: username,
            password: password
        }).select(this._fields).then(user => {
            if (user !== null) {
                return user;
            } else {
                throw new NotFoundException("Invalid username or password!");
            }
        }).catch(errUser => {
            throw errUser;
        });
    }

    async get() {
        this.users = await UserEntity.find({
            role: this._role,
        }).select(this._fields).lean();

        return this;
    }

    async build() {
        const promises = [];
        const results = [];

        this.users.forEach(user => {
            promises.push(
                results.push({
                    _id: user._id,
                    ...user.profile
                })
            );
        });

        return Promise.all(promises).then(() => {
            return results;
        });
    }
}
