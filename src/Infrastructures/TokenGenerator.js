import JWT from "jsonwebtoken";

export class TokenGenerator {

    /**
     * TokenGenerator constructor
     */
    constructor() {
        /**
         * Load .env
         */
        require("dotenv").config({
            path: "./../../../.env"
        });
    }

    /**
     * Generate token
     * @param data
     * @returns {*}
     */
    generate(data) {
        return JWT.sign({
            data: data
        }, process.env.JWT_SECRET, {
            expiresIn: "1d"
        });
    }
}