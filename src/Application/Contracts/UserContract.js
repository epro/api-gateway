export class UserContract {

	constructor(data) {
		this.username = data.username;
		this.password = data.password;
		this.role = data.role;
		this.profile = {
			name: data.name,
			email: data.email,
			phone: data.phone
		}
	}
}