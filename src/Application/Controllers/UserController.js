import { UserLogin } from "@user/Services/UserLogin";
import { UserContract } from "@contracts/UserContract";
import { UserRegister } from "@user/Services/UserRegister";
import { NotFoundException } from "@exceptions/NotFoundException";
import { UpdateUserService } from "@user/Services/UpdateUserService";
import { UserRepository } from "@infrastructures/Repositories/UserRepository";

import SuccessResponse from "@interfaces/Http/Responses/SuccessResponse";
import NotFoundResponse from "@interfaces/Http/Responses/NotFoundResponse";
import InternalServerErrorResponse from "@interfaces/Http/Responses/InternalServerErrorResponse";
import {DeleteUser} from "../../Domain/User/Services/DeleteUser";

const user = {};

user.attempt = async (req, res) => {
	try {
		const result = await new UserLogin().attempt(req.body.username, req.body.password);
		return SuccessResponse(res, "Successfully logged in!", result);
	} catch (exception) {
		console.log(exception);
        return InternalServerErrorResponse(res, "An error occurred while login!", 5);
	}
};

user.register = async(req, res) => {
	try {
		const user = new UserContract(req.body);
		const result = await new UserRegister().register(user);
		return SuccessResponse(res, "Successfully registered!", result);
	} catch (exception) {
		console.log(exception);
        return InternalServerErrorResponse(res, "An error occurred while register!", 5);
	}
};

user.index = async(req, res) => {
	try {
		const results = await new UserRepository()
			.setRole(req.query.role)
			.selectField("_id profile.name profile.phone profile.email")
			.get()
			.then(result => { return result.build(); });
		return SuccessResponse(res, "List !", results);
	} catch (exception) {
		console.log(exception);
        return InternalServerErrorResponse(res, "An error occurred while getting user data!", 5);
	}
};

user.show = async(req, res) => {
	try {
		const user = await new UserRepository()
			.selectField("_id profile.name profile.phone profile.email")
			.setRole(req.query.role)
			.profile(req.params.id);
		return SuccessResponse(res, `Detail ${req.query.role}!`, user);
	} catch (exception) {
		console.log(exception);
        if (exception instanceof NotFoundException) {
            return NotFoundResponse(res, exception.message);
        } else {
            return InternalServerErrorResponse(res, "An error occurred while getting student detail!");
        }
	}
};

user.update = async(req, res) => {
	try {
		const user = new UserContract(req.body);
		const result = await new UpdateUserService(req.params.id, user).persist();
		return SuccessResponse(res, `Detail ${req.credential.role} updated successfully!`, user);
	} catch (exception) {
		console.log(exception);
        if (exception instanceof NotFoundException) {
            return NotFoundResponse(res, exception.message);
        } else {
            return InternalServerErrorResponse(res, "An error occurred while getting student detail!");
        }
	}
};

user.del = async(req, res) => {
    try {
        const result = await new DeleteUser().delete(req.params.id);
        return SuccessResponse(res, "Sukses delete", result);
    } catch (exception) {
        return InternalServerErrorResponse(res, "Ada error")
    }
};


module.exports = user;