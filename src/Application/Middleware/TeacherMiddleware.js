import jwt from "jsonwebtoken";

/**
 * Teacher middleware authentication
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports = (req, res, next) => {

    try {
        /**
         * Load env
         */
        require("dotenv").config({
            path: "./../../../.env"
        });

        /**
         * Verify token
         */
        if (req.headers["x-source"] === "local") {
            next();
        } else {
            jwt.verify(req.headers.token, process.env.JWT_SECRET, (err, decoded) => {
                if (!err && decoded.data.role === "TEACHER") {
                    req.credential = decoded.data;
                    next();
                } else if (!err && decoded.data.role !== "TEACHER") {
                    res.status(403);
                    res.json("You are not allowed to access this feature!");
                } else {
                    throw err;
                }
            });
        }
    } catch (exception) {
        res.status(500);
        res.json(exception.message);
    }
};