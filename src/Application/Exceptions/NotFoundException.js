export class NotFoundException {

    /**
     * NotFoundException constructor
     * @param message
     */
    constructor(message) {
        this.message = message;
    }
}