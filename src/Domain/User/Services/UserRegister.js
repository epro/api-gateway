import UserEntity from  "@user/Entities/UserEntity";
import { TokenGenerator } from "@infrastructures/TokenGenerator";

export class UserRegister {

    async register(data) {
        this.data = data;
        const result = await this.storeUser();
        const token = new TokenGenerator().generate({
            "id": result._id,
            "reference_id": result.profile._id,
            "name": result.profile.name,
            "role": result.role
        });

        this.storeToken(result, token);
        return {
            id: result._id,
            name: result.profile.name,
            email: data.profile.email,
            phone: data.profile.phone,
            username: result.username,
            role: result.role,
            token: token
        };
    }

    /**
     * User register data
     * @returns {Promise<T>}
     */
    async storeUser() {
        return UserEntity.create(this.data).then(created => {
            return created;
        }).catch(errCreated => {
            throw errCreated;
        });
    }

    /**
     * Store token to DB
     * @param user_id
     * @param token
     */
    storeToken(user, token) {
        user.token = token;
        user.save();
    }
}