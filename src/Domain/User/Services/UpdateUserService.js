import _ from "lodash";
import UserEntity from "@user/Entities/UserEntity";

export class UpdateUserService {

	constructor(id, data) {
		this._id = id;
		this._data = _.pickBy(data, _.identity); // This wil remove any properties with null or undefined value
	}

	async persist() {
		return UserEntity.findByIdAndUpdate(this._id, {
			$set: this._data
		}, {
			new: true
		}).then(updated => {
			if (updated !== null) {
				return updated;
			} else {
				throw new NotFoundException("The data you're looking for couldn't be found!");
			}
		});
	}
}