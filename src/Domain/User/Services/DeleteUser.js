import UserEntity from "@user/Entities/UserEntity";

export class DeleteUser {
    async delete(user_id) {
        return UserEntity.findByIdAndRemove({
            _id: user_id
        }).then(removed => {
            return removed;
        }).catch(error => {
            return error;
        });
    }
}