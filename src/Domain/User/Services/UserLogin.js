import UserEntity from "@user/Entities/UserEntity";
import { TokenGenerator } from "@infrastructures/TokenGenerator";
import { UserRepository } from "@infrastructures/Repositories/UserRepository";

export class UserLogin {

	async attempt(username, password) {
		const user = await new UserRepository().selectField("-created_at -__v -updated_at -deleted_at -token").findByCredential(username, password);
		const token = new TokenGenerator().generate({
            "id": user._id,
            "reference_id": user.profile._id,
            "name": user.name,
            "role": user.role
        });

        this.storeToken(user._id, token);
		return {
			id: user._id,
            name: user.profile.name,
            email: user.profile.email,
            phone: user.profile.phone,
            username: user.username,
            role: user.role,
            token: token
		};
	}

	storeToken(user_id, token) {
		UserEntity.update(
            { user_id: user_id },
            { user_id: user_id, token: token },
            { upsert: true, setDefaultsOnInsert: true }
        );
	}
}