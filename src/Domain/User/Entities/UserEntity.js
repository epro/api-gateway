import mongoose from "mongoose";
import UserProfileEntity from "@user/Entities/UserProfileEntity";

const userProfile = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    deleted_at: Date
});

/**
 * Defining user schema
 */
const user = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ["ADMIN", "STUDENT", "TEACHER"],
        required: true
    },
    token: String,
    profile: userProfile,
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    deleted_at: Date
});

/**
 * Creating user model
 * @type {Model}
 */
module.exports = mongoose.model("User", user);