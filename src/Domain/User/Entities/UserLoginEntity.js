import mongoose from "mongoose";

/**
 * Defining user login schema
 */
const userLogin = new mongoose.Schema({
    user_id: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});

/**
 * Creating user login model
 * @type {Model}
 */
const UserLogin = mongoose.model("UserLogin", userLogin);

/**
 * Exporting user login model
 * @type {Model}
 */
module.exports = UserLogin;