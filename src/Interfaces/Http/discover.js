import rp from "request-promise";
import servers from "@config/servers";

async function discover(server, key) {
    const promises = [];

    server.map((item, index) => {
        promises.push(
            rp(item.url).then(() => {
                // console.log(`${item.url} is REACHABLE!`);
                servers[process.env.APP_ENV][key][index].status = true;
            }).catch(() => {
                // console.log(`${item.url} is UNREACHABLE!`);
                servers[process.env.APP_ENV][key][index].status = false;
            })
        );
    });

    return Promise.all(promises);
}

module.exports = () => {
    setInterval(() => {
        const promises = [];
        Object.keys(servers[process.env.APP_ENV]).map((key) => {
            promises.push(discover(servers[process.env.APP_ENV][key], key));
        });

        Promise.all(promises).then(() => {
        }).catch(error => {
            console.log(error);
            console.error("An error occurred while discover servers!");
        })
    }, process.env.INTERVAL_DISCOVERY || 3000);
};