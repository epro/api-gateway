import _ from "lodash";
import rp from "request-promise";
import servers from "@config/servers";

let current = 0;

module.exports = (req, res) => {
    const available_servers = _.filter(servers[process.env.APP_ENV].test, server => {
        return server.status === true;
    });

    let endpoint = req.url.replace(/\/admin|\/student|\/teacher/gi, "");

    let url = "";
    if (endpoint === "/test") {
        url = available_servers[current].url.replace("/test", "") + endpoint + "/"; 
    } else {
        url = available_servers[current].url + endpoint;
    }

    const options = {
        method: req.method,
        url: url,
        headers: {
            "user-id": req.credential.id,
            "role": req.credential.role,
            "id": req.credential.reference_id,
            "token": req.headers.token
        },
        body: req.body,
        json: true
    };

    if (available_servers.length >= 1) {
        current++;
        if (current === available_servers.length) current = 0;

        rp(options).then(result => {
            res.json(result);
        }).catch(error => {
            res.status(error.statusCode);
            res.json(error.error);
        });
    } else {
        res.status(502);
        res.json("All test servers are currently unreachable!");
    }
};