import UserController from "@controllers/UserController";
import AdminMiddleware from "@middleware/AdminMiddleware";
import Authenticate from "@middleware/Authenticate";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance();

router.post("/login", UserController.attempt);
router.post("/register", UserController.register);
router.get("/", AdminMiddleware, UserController.index);
router.get("/:id", Authenticate, UserController.show);
router.put("/:id", AdminMiddleware, UserController.update);
router.del("/:id", Authenticate, UserController.del);

module.exports = router;