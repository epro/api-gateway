import TestHandler from "@interfaces/Http/Handler/test";
import ClassHandler from "@interfaces/Http/Handler/classes";
import MaterialHandler from "@interfaces/Http/Handler/material";
import AdminMiddleware from "@middleware/AdminMiddleware";
import StudentMiddleware from "@middleware/StudentMiddleware";
import TeacherMiddleware from "@middleware/TeacherMiddleware";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance();

router.get("/", (req, res) => {
	res.json("API Gateway UP!");
});

/**od

 * Class routers
 */
router.get("/admin/class*", [AdminMiddleware, ClassHandler]);
router.put("/admin/class*", [AdminMiddleware, ClassHandler]);
router.post("/admin/class*", [AdminMiddleware, ClassHandler]);
router.del("/admin/class*", [AdminMiddleware, ClassHandler]);

router.get("/student/class*", [StudentMiddleware, ClassHandler]);
router.post("/student/class*", [StudentMiddleware, ClassHandler]);

router.get("/teacher/class*", [TeacherMiddleware, ClassHandler]);
router.put("/teacher/class*", [TeacherMiddleware, ClassHandler]);
router.post("/teacher/class*", [TeacherMiddleware, ClassHandler]);

/**
 * Test routers
 */
router.get("/admin/test*", [AdminMiddleware, TestHandler]);
router.put("/admin/test*", [AdminMiddleware, TestHandler]);
router.post("/admin/test*", [AdminMiddleware, TestHandler]);

router.get("/student/test*", [StudentMiddleware, TestHandler]);
router.put("/student/test*", [StudentMiddleware, TestHandler]);
router.post("/student/test*", [StudentMiddleware, TestHandler]);

router.del("/teacher/test*", [TeacherMiddleware, TestHandler]);
router.get("/teacher/test*", [TeacherMiddleware, TestHandler]);
router.put("/teacher/test*", [TeacherMiddleware, TestHandler]);
router.post("/teacher/test*", [TeacherMiddleware, TestHandler]);

router.get("/teacher/question*", [TeacherMiddleware, TestHandler]);
router.put("/teacher/question*", [TeacherMiddleware, TestHandler]);
router.post("/teacher/question*", [TeacherMiddleware, TestHandler]);

/**
 * Material routers
 */
router.get("/admin/material*", [AdminMiddleware, MaterialHandler]);
router.put("/admin/material*", [AdminMiddleware, MaterialHandler]);
router.post("/admin/material*", [AdminMiddleware, MaterialHandler]);

router.get("/student/material*", [StudentMiddleware, MaterialHandler]);
router.put("/student/material*", [StudentMiddleware, MaterialHandler]);
router.post("/student/material*", [StudentMiddleware, MaterialHandler]);

router.del("/teacher/material*", [TeacherMiddleware, MaterialHandler]);
router.get("/teacher/material*", [TeacherMiddleware, MaterialHandler]);
router.put("/teacher/material*", [TeacherMiddleware, MaterialHandler]);
router.post("/teacher/material*", [TeacherMiddleware, MaterialHandler]);

router.add("/user", require("./User"));

module.exports = router;